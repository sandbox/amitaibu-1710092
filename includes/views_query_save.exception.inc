<?php

/**
 * @file
 * Provide a separate Exception so it can be caught separately.
 */

class ViewsQuerySaveException extends Exception {}
