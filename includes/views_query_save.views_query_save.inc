<?php

/**
 * @file
 * A class used for messages.
 */


class ViewsQuerySave extends Entity {

  /**
   * The name of the views query.
   *
   * @var string
   */
  public $name;

  /**
   * The message arguments.
   *
   * @var array
   */
  public $filters = array();

  /**
   * TRUE if the view query is published.
   *
   * @var boolean
   */
  public $status = TRUE;

  public function __construct($values = array()) {
    $values += array(
      'created' => time(),
    );
    parent::__construct($values, 'views_query_save');
  }
}
